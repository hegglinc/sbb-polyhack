from data import Data
from datetime import datetime, timedelta


class Score:
    def __init__(self, line, date):
        self.line = line
        self.date = datetime.strptime(date, "%Y-%m-%d")
        self.constant = 1.1

    def calcReduction(self):
        stations = self.line['stations']
        constructions = self.line['constructions']
        station_reduction = []
        station_reduction_reversed = []
        curr_reduction = 1
        count = 0
        for station in stations:
            id = station['id']
            for construct in constructions:
                if id == construct['station_to'] or id == construct['station_from']:
                    date_from = datetime.strptime(construct['date_from'], "%Y-%m-%d")
                    date_to = datetime.strptime(construct['date_to'], "%Y-%m-%d")
                    if self.date > date_from and self.date > date_to:
                        curr_reduction *= 1-construct['reduction']
                    #print(construct['reduction'])
            curr_reduction *= self.constant
            curr_reduction = min(1, curr_reduction)
            if count != 0:
                station_reduction.append(curr_reduction)
            count += 1
        count = 0
        curr_reduction = 1
        for station in reversed(stations):
            id = station['id']
            for construct in constructions:
                if id == construct['station_to'] or id == construct['station_from']:
                    curr_reduction *= 1-construct['reduction']
            curr_reduction *= self.constant
            curr_reduction = min(1, curr_reduction)
            if count != 0:
                station_reduction_reversed.append(curr_reduction)
            count += 1
        #print(station_reduction)
        station_reduction_reversed.reverse()
        #print(station_reduction_reversed)
        station_reduction_final = []
        n = len(station_reduction)
        assert n == len(station_reduction_reversed )
        for i in range (0,n):

            station_reduction_final.append((station_reduction_reversed[i] + station_reduction[i]) / 2)
        return station_reduction_final
if __name__ == '__main__':
    lines = Data('resources/linie-mit-betriebspunkten.csv', 'resources/construction-site.csv').getLines()
    score = Score(lines[0], "2024-06-22")
    score.calcReduction()
