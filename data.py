import csv

class Data:
    def __init__(self, linedata, constructiondata):
        self.linedata = linedata
        self.constructiondata = constructiondata

    def getConstruction(self):
        constructions = []
        with open(self.constructiondata) as csvfile:
            next(csvfile)
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                station_from = row[2]
                station_to = row[3]

                pos = row[13]
                try: Epos = float(pos.split(',')[0])
                except: Epos = float('nan')
                try: Npos = float(pos.split(',')[1])
                except: Npos = float('nan')

                try: km_from = float(row[4])
                except: km_from = float('nan')
                try: km_to = float(row[5])
                except: km_to = float('nan')

                date_from = row[8]
                date_to = row[9]

                try: reduction = float(row[12])
                except: reduction = float('nan')

                reduction_day = (row[11] == 'Sperre Strecke Tag' or row[11] == 'Umsetzung')
                reduction_night = (row[11] == 'Sperre Strecke Nacht' or row[11] == 'Umsetzung')
                constructions.append({'station_to':station_to, 'station_from':station_from, 'Epos':Epos, 'Npos':Npos,
                                     'km_from':km_from, 'km_to':km_to, 'date_from':date_from, 'date_to':date_to,
                                     'reduction':reduction})
        return constructions

    def getLines(self):
        lines = {}
        # Do the lines thing
        with open(self.linedata) as csvfile:
            next(csvfile)
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                id = row[4]
                name = row[6]
                lines[id] = {'id':id, 'name':name,'stations':{}, 'constructions':[]}

        # Add the stations
        with open(self.linedata) as csvfile:
            next(csvfile)
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                lineid = row[4]
                stationid = row[0]
                name = row[1]

                try: km = float(row[5])
                except: km = float('nan')

                pos = row[10]
                try: Epos = float(pos.split(',')[0])
                except: Epos = float('nan')
                try: Npos = float(pos.split(',')[1])
                except: Npos = float('nan')

                lines[lineid]['stations'][stationid] = {'id':stationid, 'name': name, 'Npos': Npos, 'Epos': Epos, 'km': km}

        # Add the construction to the lines
        constructions = self.getConstruction()
        for line_id, line in lines.items():
            for construct in constructions:
                station_from = construct['station_from']
                station_to = construct['station_to']
                if station_to in line['stations'] and station_from in line['stations']:
                    line['constructions'].append(construct)
            line['constructions'] = sorted(line['constructions'], key=lambda constr: constr['km_from'])

        linelist = list(lines.values())
        for line in linelist:
            line['stations'] = self.sortStations(line['stations'])
        return linelist

    def sortStations(self, stations):
        newstations = stations.copy()
        for station_id, station in newstations.items():
            station['id'] = station_id
        return sorted(newstations.values(), key=lambda station: station['km'])

if __name__ == '__main__':
    linedata = 'resources/linie-mit-betriebspunkten.csv'
    constructiondata = 'resources/construction-site.csv'
    data = Data(linedata, constructiondata)
    lines = data.getLines()

